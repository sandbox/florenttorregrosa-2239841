<?php

/**
 * Admin form for banned projects.
 */
function banned_projects_settings_form() {
  $banned_list = _banned_projects_set_modules_list();

  $form = array();

  $form['banned_modules'] = array(
    '#type' => 'checkboxes',
    '#options' => drupal_map_assoc($banned_list),
    '#default_value' =>  variable_get('banned_modules', array()),
    '#title' => t('Check the modules you do not want to be enabled.'),
  );

  $form['#submit'][] = 'banned_projects_settings_form_submit';

  return system_settings_form($form);
}

/**
 * Submit handler for banned_projects_settings_form().
 */
function banned_projects_settings_form_submit($form, &$form_state) {
  $prepared_banned_modules = array();

  foreach ($form_state['values']['banned_modules'] as $module) {
    if ($module != '') {
      $module_info = explode('**', $module);
      $prepared_banned_modules[] = array(
        'package'             => $module_info[0],
        'module_machine_name' => $module_info[1],
      );
    }
  }

  variable_set('prepared_banned_modules', $prepared_banned_modules);
}

/**
 * Helper function to prepare the values for banned_projects_settings_form().
 */
function _banned_projects_set_modules_list() {
// Get current list of modules.
  $files = system_rebuild_module_data();
  // Remove hidden modules from display list.
  $visible_files = $files;
  foreach ($visible_files as $filename => $file) {
    if (!empty($file->info['hidden'])) {
      unset($visible_files[$filename]);
    }
  }

  $banned_list = array();
  foreach ($visible_files as $module_machine_name) {
    $banned_list[] = $module_machine_name->info['package'] . '**' . $module_machine_name->name;
  }

  return $banned_list;
}